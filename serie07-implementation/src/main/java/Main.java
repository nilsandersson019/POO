import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
	private final static int listSize = 10000;
	private final static int iterationSize = 1000000; // 600000000 is a good number to see the linked list become faster

	private static int selectmilion(List<Integer> list, int[] random) {
		int sum = 0;
		for(int i = 0; i < iterationSize; i++) {
			sum += list.get(random[i]);
		}
		return sum;
	}

	public static void main(String[] args) {
		ArrayList<Integer> aList = new ArrayList<>();
		LinkedList<Integer> lList = new LinkedList<>();

		for(int i = 0; i < listSize; i++) {
			Integer randomNum = ThreadLocalRandom.current().nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
			aList.add(randomNum);
			lList.add(randomNum);
		}

		//Pregenerate the pseudorandom sequence so we don't test the random function
		int[] random = new int[iterationSize];
		for(int i = 0; i < iterationSize; i++) {
			ThreadLocalRandom.current().nextInt(0, listSize);
		}

		long timer = System.nanoTime();
		selectmilion(aList, random);
		System.out.println("ArrayList integration time : " + ((System.nanoTime() - timer) / 1e6) + " ms.");

		timer = System.nanoTime();
		selectmilion(lList, random); // When way above 1e6, the linked list becomes strangely the fastest but it doesn't make sense to me because the get complexity of linkedlist should be O(n) and for arraylist it should be O(1
		System.out.println("LinkedList integration time : " + ((System.nanoTime() - timer) / 1e6) + " ms.");
	}
}
