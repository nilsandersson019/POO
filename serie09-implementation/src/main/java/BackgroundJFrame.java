import javax.swing.*;
import java.awt.*;

public class BackgroundJFrame extends JFrame {

	BackgroundJFrame() {
		super();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();

		panel.setPreferredSize(new Dimension(600, 300));


		JButton green = new JButton();
		JButton red = new JButton();
		JButton blue = new JButton();

		green.setText("green");
		red.setText("red");
		blue.setText("blue");

		green.addActionListener(actionEvent -> panel.setBackground(Color.green));
		red.addActionListener(actionEvent -> panel.setBackground(Color.red));
		blue.addActionListener(actionEvent -> panel.setBackground(Color.blue)  );

		green.addActionListener(actionEvent -> this.setTitle("Green Colored Window"));
		red.addActionListener(actionEvent -> this.setTitle("Red Colored Window"));
		blue.addActionListener(actionEvent -> this.setTitle("Blue Colored Window"));


		panel.add(green);
		panel.add(red);
		panel.add(blue);
		this.add(panel);


		pack();
		validate();
		setVisible(true);
	}
}
