import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseEventDemo extends JFrame implements MouseListener {

	static JLabel pane;


	MouseEventDemo(int width, int height) {

		setPreferredSize(new Dimension(width, height));
		JPanel p = new JPanel();
		setTitle("no workerino");
		p.setSize(new Dimension(width, height));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		pane = new JLabel("thisjhfhfe");
		pane.setSize(width, height / 2);

		p.add(pane);
		this.add(p);

		p.addMouseListener(this);

		pack();
		validate();
		setVisible(true);

	}


	@Override
	public void mouseClicked(MouseEvent mouseEvent) {
		pane.setText((pane.getText() + "\n  Mouse clicked"));
	}

	@Override
	public void mousePressed(MouseEvent mouseEvent) {
		pane.setText((pane.getText() + "\n  Mouse pressed"));

	}

	@Override
	public void mouseReleased(MouseEvent mouseEvent) {
		pane.setText((pane.getText() + "\n  Mouse released"));
	}

	@Override
	public void mouseEntered(MouseEvent mouseEvent) {
		pane.setText((pane.getText() + "\n  Mouse entered"));
	}

	@Override
	public void mouseExited(MouseEvent mouseEvent) {
		pane.setText((pane.getText() + "\n  Mouse exited"));

	}
}
