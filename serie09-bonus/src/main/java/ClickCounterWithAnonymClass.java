import javax.swing.*;

public class ClickCounterWithAnonymClass {


		public static void main(String[] args) {
			init();
		}

		static JPanel panel;
		static int counter = 0;

		public static void init() {
			JFrame frame = new JFrame();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.add(content());
			frame.pack();
			frame.setVisible(true);
		}

		private static JPanel content() {
			panel = new JPanel();
			JLabel label = new JLabel();

			JButton button = new JButton("click");
			button.addActionListener(e -> counter(1));
			label.setText("The Counter has been clicked " + counter + " times");

			panel.add(label);
			panel.add(button);

			return panel;

		}

		private static int counter(int clicks) {


			for (int i = 0; i >= clicks; i++) {
				counter = counter++;
			}

			return counter;
		}

//	public void refresh() {
//	}

		public void run() {

		}
	}

