import java.util.ArrayList;

public class FizzBuzz {

	static ArrayList<Integer> numbers = new ArrayList<Integer>();

	public FizzBuzz(ArrayList<Integer> numbers) {

		this.numbers  = numbers;
		setNumbers();
	}

	public  ArrayList<Integer> setNumbers() {

		for (int i = 1; i <= 50; i++) {
			numbers.add(i);
		}

		return numbers;
	}

	public ArrayList<Integer> getNumbers() {
		return numbers;
	}


	public String printNumbers() {

		String concatenated = null;
		while(!numbers.isEmpty()){
			concatenated = concatenated + "\n" + numbers.get(numbers.size());
			numbers.remove(numbers.size());
		}

		return concatenated;
	}
}
