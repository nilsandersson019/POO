public class Main {

    public static void main(String[] args) {
        Box test1 = new Box<String,Double>("ihfvbshlbf",5.732732);
        Box test2 = new Box<String,Double>("carl",Math.PI);
        Box test3 = new Box<String,String>("Carl","Sagan");

        test1.print();
        test2.print();

        test1.swapItemsWith(test2);
        test2.print();

        test1.moveFirstTo(test2);
        test2.print();

        test1.moveItemsFrom(test2);
        test1.print();

        test3.swapItems(test3);
        test3.print();

        test1.swapItems(test3);
        test3.print();

        test3.moveFirstToSecond(test3);
        test3.print();
    }
}
