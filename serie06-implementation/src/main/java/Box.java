public class Box<F, S> {

    F first;
    S second;

    Box(F first, S second) {
        this.first = first;
        this.second = second;
    }

    void moveFirstTo(Box<F, Object> box){
        box.first  = this.first;
    }

    void moveItemsFrom(Box<F, S> box){
        this.first = box.first;
        this.second = box.second;
    }

    void swapItemsWith(Box<F, S> box){
        Box<F,S> temp = new Box<>(first, second);

        this.first = box.first;
        this.second = box.second;

        box.first = temp.first;
        box.second = temp.second;
    }


    void moveFirstToSecond(Box<F, F> box){
        box.second = box.first;
    }

    void swapItems(Box<F,F> box){
        F f;
        f = box.first;
        box.first = box.second;
        box.second = f;
    }
    void print(){
        System.out.println("first: " + first + "\nsecond: " + second);
    }
}
