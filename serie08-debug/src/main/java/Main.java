import java.io.IOException;

public class Main {
	public static void main(String[] args) {
		foo();
		bar();
	}

	static private void foo() {
		throw new IllegalStateException();
	}

	static private void bar() {
		try {
			throw new IOException("d");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}


