package Geometry;

import Tools.Vector;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

public class PolygonTest {

	public PolygonTest() {
	}

	@Test
	public void toSegments() {
	}

	@Test
	public void contains() {
		Vector[] vects = new Vector[5];
		vects[0] = new Vector(0, 0.000000001);
		vects[1] = new Vector(-5, 5);
		vects[2] = new Vector(-10, 0);
		vects[3] = new Vector(-12, 54);
		vects[4] = new Vector(15, 18);

		// a  new Tools.Vector(-1, -1); breaks it, probably because it goes exactly through a summit of the polygon( and thus goes through 2 segments instead of one)

		Geometry.Polygon p = new Geometry.Polygon(vects, Color.BLACK);

		assertFalse(p.contains(new Geometry.Point(8000, 2134987)));
		assertFalse(p.contains(new Geometry.Point(-10, -10)));
		assertTrue(p.contains(new Point(-5, 10)));
	}

	@Test
	public void duplicate() {
	}

	@Test
	public void nbVertices() {
	}

	@Test
	public void getVertex() {
	}

	@Test
	public void area() {
	}

	@Test
	public void centroid() {
	}

	@Test
	public void translate() {
	}

	@Test
	public void rotate() {
	}

	@Test
	public void scale() {
	}

	@Test
	public void paint() {
	}

	@Test
	public void verticesAsArrays() {
	}
}