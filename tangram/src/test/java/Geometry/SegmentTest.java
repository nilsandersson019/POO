package Geometry;

import org.junit.Test;

import static org.junit.Assert.*;

public class SegmentTest {

	public SegmentTest() {
	}

	@Test
	public void length() {
	}

	@Test
	public void slope() {
	}

	@Test
	public void intersects() {
		Segment x = new Segment(new Point(0,0), new Point(1,1));
		Segment y = new Segment(new Point(0,1), new Point(1,0));
		Segment z = new Segment(new Point(2,1), new Point(6,0));
		Segment zz = new Segment(new Point(0,1), new Point(1,2));
		System.out.println("simple intersection, not parallel");
		assertTrue(x.intersects(y));
		System.out.println("no intersection, not parallel");
		assertFalse(x.intersects(z));
		System.out.println("no intersection, parallel");
		assertFalse(x.intersects(zz));
		System.out.println("intersection, parallel");
		assertTrue(x.intersects(x));
	}
}