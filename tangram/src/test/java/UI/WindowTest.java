package UI;

import Geometry.Point;
import Geometry.Tan;
import Tools.LinkedList;
import Tools.Vector;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

public class WindowTest {

	private LinkedList<Tan> shapeList;

	private Geometry.Polygon rectangle1, rectangle2, rectangle3, triangle1, pentagon1;
	private UI.Window w;

	public WindowTest() {
		Vector[] rectPath1 = Vector.rectanglePath(2, 1);
		Vector[] rectPath2 = Vector.rectanglePath(1, 0.6);
		Vector[] trigPath1 = Vector.trianglePath(2, 0.757, 0);
		Vector[] pentPath1 = Vector.pentagonPath(0.5);

		rectangle1 = new Geometry.Polygon(rectPath1, Color.YELLOW, new Geometry.Point(0, 0));
		rectangle2 = new Geometry.Polygon(rectPath2, Color.CYAN, new Geometry.Point(1.2, 1.2));
		rectangle3 = new Geometry.Polygon(rectPath2, Color.GREEN, new Geometry.Point(-1.2, -1.2));
		triangle1 = new Geometry.Polygon(trigPath1, Color.RED, new Geometry.Point(-0, 0));
		pentagon1 = new Geometry.Polygon(pentPath1, Color.BLUE, new Point(0, 0));

		shapeList = new LinkedList<Tan>();

		shapeList.add(new Tan(rectangle1));
		shapeList.add(new Tan(rectangle2));
		shapeList.add(new Tan(rectangle3));
		shapeList.add(new Tan(triangle1));
		shapeList.add(new Tan(pentagon1));

		w = new Window(shapeList, 800, 600);
	}

	@Test
	public void getTans() {
	}
}