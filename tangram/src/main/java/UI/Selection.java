package UI;

import Geometry.Point;
import Geometry.Tan;
import Tools.Rotator;
import Tools.TanList;
import Tools.Transformer;
import Tools.Translator;

import static UI.View.STEP_SIZE;


public class Selection {
	private static Selection instance;
	private static Tan selectedTan;
	private Transformer transformer = null;
	private Window w;


	private Selection() {
	}

	public static Selection instance() {
		if (instance == null) {
			instance = new Selection();
		}
		return instance;
	}

	public void init(Tan t, Window w) {
		selectedTan = t;
		this.w = w;
	}

	public Tan getSelectedTan() {
		return selectedTan;
	}

	public void setSelectedTan(Point p) {
		for (Tan t : TanList.getInstance().getShapeList()) {
			if (t.contains(p)) {
				selectedTan = t;
			}
		}
	}


	public void controlMoved(Point mousePos, Point newPos) {
//		 Geometry.Point mousePos  = new Geometry.Point(MouseInfo.getPointerInfo().getLocation().x,MouseInfo.getPointerInfo().getLocation().y );
		if (selectedTan.isOnSpot(mousePos)) {
			Translator.getInstance().controlMoved(null, newPos);
		}
		else if (selectedTan.contains(mousePos) && !selectedTan.isOnSpot(mousePos)) {
			System.out.println("We're rotating !");
			Rotator.getInstance().controlMoved(mousePos, newPos);
		}
		w.refresh();
	}
}
