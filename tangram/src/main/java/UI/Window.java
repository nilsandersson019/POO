package UI;

import Geometry.Polygon;
import Geometry.Tan;
import Tools.LinkedList;
import Runnable.Main;
import Tools.TanList;

import javax.swing.*;
import java.awt.*;

public class Window extends JFrame {

	Controller controller;

	private LinkedList<Tan> tans;


	private View view;
	private JPanel panel;

	public Window(int height, int width) throws HeadlessException {
		super();
		LinkedList<Tan> tans = TanList.getInstance().getShapeList();
		Polygon puzzleField = TanList.getInstance().getPuzzleField();

		Main.setWindow(this);

		this.tans = tans;
		Selection.instance().init(tans.get(0), this);

		setTitle("Tangram");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		JMenuBar bar = new JMenuBar();
		JMenu game = new JMenu();
		JMenuItem exit = new JMenuItem();
		JMenuItem options = new JMenuItem();


		game.setText("Game");
		exit.setText("Exit");
		exit.addActionListener(actionEvent -> System.exit(0));
		options.setText("About");
		options.addActionListener(actionEvent -> new AboutWindow());
		game.add(options);
		game.add(exit);
		bar.add(game);

		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setPreferredSize(new Dimension(width, height));


		panel.add(bar);
		view = new View(width, height, this.tans, puzzleField);
		panel.add(view);

		panel.setBackground(new Color(60, 60, 60));

		setContentPane(panel);

		controller = new Controller();
		this.addMouseListener(controller);
		this.addKeyListener(controller);

		pack();
		validate();
		setVisible(true);

	}

	public void refresh() {
		setContentPane(panel);
		if (view.isPuzzleCompleted()){
			System.out.println("great success");
		}
		pack();
		validate();
		setVisible(true);
	}
	//might need
//	protected void defineCoordinates(int xOrigin, int yOrigin, double scale, boolean upwardYAxis) {
//		view.setOrigin(xOrigin, yOrigin);
//		view.setScale(scale, upwardYAxis);
//	}

	public final LinkedList<Tan> getTans() {
		return tans;
	}

	public View getView() {
		return view;
	}

}
