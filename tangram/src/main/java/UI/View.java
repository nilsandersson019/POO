package UI;

import Geometry.Polygon;
import Geometry.Tan;
import Tools.LinkedList;
import oop.lib.Display;
import oop.lib.Paintable;

import java.awt.*;

import static Runnable.Main.FIELD_WIDTH;


public class View extends Display {

	private LinkedList<Tan> tanList;
	private Polygon puzzleField;
	public static final int STEP_SIZE = FIELD_WIDTH/50;

	public View(int width, int height, LinkedList<Tan> viewList, Polygon puzzleField) {
		super(width, height);
		this.puzzleField = puzzleField;
		this.tanList = viewList;
		setPreferredSize(new Dimension(width, height));
		setBackground(new Color(255, 255, 200));
	}

	@Override
	public void paint(Display display) {
		puzzleField.paint(this);
		for (Paintable p : tanList) {
			p.paint(this);
		}
	}

	public boolean isPuzzleCompleted() { //TODO: make this work
		for (Tan t : tanList) {
			if(!t.isAloneInPuzzleField(puzzleField)) {
				return false;
			}
		}

		return true;
	}

	public int getXOrigin() {
		return super.getXOrigin();
	}

	public int getYOrigin() {
		return super.getYOrigin();
	}

	public double getXScale() {
		return super.getXScale();
	}

	public double getYScale() {
		return super.getYScale();
	}
}
