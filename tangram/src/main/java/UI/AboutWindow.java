package UI;

import javax.swing.*;
import java.awt.*;

public class AboutWindow extends JFrame {

	AboutWindow() {
		super();
		setTitle("Tangram");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setPreferredSize(new Dimension(300, 100));
		setContentPane(panel);

		panel.add(new JTextArea("Software by Nils Andersson\n" +
				"Object Oriented Programming class\n" +
				"University of Fribourg, CH\n" +
				"(C) 2018 - Prof. Rolf Ingold"));

		pack();
		validate();
		setVisible(true);
	}

}
