package Tools;

import Geometry.Point;
import UI.Selection;

public class Translator extends Transformer {

	private static Translator instance;

	private Translator() {
	}

	public static Translator getInstance() {
		if (instance == null) {
			instance = new Translator();
		}
		return instance;
	}

	@Override
	public void controlMoved(Point unused, Point pt) {
		System.out.println("inside translator");
//		double xTrans = STEP_SIZE * Math.round((prevPoint.x() - pt.x()) / STEP_SIZE);
//		double yTrans = STEP_SIZE * Math.round((prevPoint.y() - pt.y()) / STEP_SIZE);
		double xTrans = pt.x() - Selection.instance().getSelectedTan().centroid().x();
		double yTrans = pt.y() - Selection.instance().getSelectedTan().centroid().y();

		Selection.instance().getSelectedTan().translate(xTrans, yTrans);
	}

}
