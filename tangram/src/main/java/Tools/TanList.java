package Tools;

import Geometry.Polygon;
import Geometry.Tan;
import UI.Selection;

import java.awt.*;

import static Runnable.Main.FIELD_WIDTH;
import static Runnable.Main.MARGIN;
import static UI.View.STEP_SIZE;

public class TanList {

	private LinkedList<Tan> shapeList;

	private Polygon puzzleField;

	private static TanList ourInstance = new TanList();

	public static TanList getInstance() {
		return ourInstance;
	}

	private TanList() {
		Vector[] puzzlePath = Vector.squarePath(FIELD_WIDTH);
		puzzleField = new Polygon(puzzlePath, Color.lightGray, new Geometry.Point((FIELD_WIDTH + MARGIN) / 2, (FIELD_WIDTH + MARGIN) / 2));

		Vector[] trigPath1 = Vector.rectTrianglePath((Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 2) - 1, (Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 2) - 1);
		Vector[] trigPath2 = Vector.rectTrianglePath((Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 2) - 1, (Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 2) - 1);
		Vector[] trigPath3 = Vector.rectTrianglePath((FIELD_WIDTH / 2) - 1, (FIELD_WIDTH / 2) - 1);
		Vector[] trigPath4 = Vector.rectTrianglePath((Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 4) - 1, (Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 4) - 1);
		Vector[] trigPath5 = Vector.rectTrianglePath(Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 4, Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 4);
		Vector[] squarePath1 = Vector.squarePath((Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 4) - 1);
		Vector[] parallelogramPath1 = Vector.tanParalellogramPath((Math.sqrt((Math.pow((FIELD_WIDTH), 2) + (Math.pow((FIELD_WIDTH), 2)))) / 4) - 1);

		Geometry.Polygon triangle1 = new Geometry.Polygon(trigPath1, Color.darkGray, new Geometry.Point(FIELD_WIDTH + MARGIN + 5*STEP_SIZE, MARGIN + 10 * STEP_SIZE));
		Geometry.Polygon triangle2 = new Geometry.Polygon(trigPath2, Color.RED, new Geometry.Point(FIELD_WIDTH + MARGIN + 3 * STEP_SIZE, MARGIN + 20 * STEP_SIZE));
		Geometry.Polygon triangle3 = new Geometry.Polygon(trigPath3, Color.green, new Geometry.Point(FIELD_WIDTH + MARGIN + 4 * STEP_SIZE, MARGIN + 6 * STEP_SIZE));
		Geometry.Polygon triangle4 = new Geometry.Polygon(trigPath4, Color.BLUE, new Geometry.Point(FIELD_WIDTH + MARGIN + 15 * STEP_SIZE, MARGIN + 15 * STEP_SIZE));
		Geometry.Polygon triangle5 = new Geometry.Polygon(trigPath5, Color.BLUE, new Geometry.Point(FIELD_WIDTH + MARGIN + 6 * STEP_SIZE, MARGIN + 20 * STEP_SIZE));
		Geometry.Polygon square1 = new Geometry.Polygon(squarePath1, Color.magenta, new Geometry.Point(FIELD_WIDTH + MARGIN + 21 * STEP_SIZE, MARGIN + 2 * STEP_SIZE));
		Geometry.Polygon parallelogram1 = new Geometry.Polygon(parallelogramPath1, Color.pink, new Geometry.Point(FIELD_WIDTH + MARGIN + 6 * STEP_SIZE, MARGIN + 14 * STEP_SIZE));


		shapeList = new LinkedList<>();


		shapeList.add(new Tan(triangle1));
		shapeList.add(new Tan(triangle2));
		shapeList.add(new Tan(triangle3));
		shapeList.add(new Tan(triangle4));
		shapeList.add(new Tan(triangle5));
		shapeList.add(new Tan(square1));
		shapeList.add(new Tan(parallelogram1));
	}

	public LinkedList<Tan> getShapeList() {
		return shapeList;
	}

	public void updateList() {
		Tan tmp = Selection.instance().getSelectedTan();
		shapeList.remove(tmp);
		shapeList.add(tmp);
	}

	public Polygon getPuzzleField() {
		return puzzleField;
	}


}
