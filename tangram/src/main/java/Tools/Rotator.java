package Tools;

import Geometry.Point;
import UI.Selection;

public class Rotator extends Transformer {
	private static Rotator instance;

	private Rotator() {
	}

	public static Rotator getInstance() {
		if(instance == null) {
			instance = new Rotator();
		}
		return instance;
	}

	@Override
	public void controlMoved(Point oldPoint, Point newPoint) {
		Point tanCenter = Selection.instance().getSelectedTan().centroid();

		double sign;
		//     | x
		//  -------
		//     |
		if (newPoint.x() >= tanCenter.x() && newPoint.y() >= tanCenter.y()) {
			if(oldPoint.x() < newPoint.x() || oldPoint.y() > newPoint.y()) {
				sign = -1.f;
			}
			else {
				sign = 1.f;
			}
		}
		//     |
		//  -------
		//     | x
		else if (newPoint.x() <= tanCenter.x() && newPoint.y() >= tanCenter.y()) {
			if(oldPoint.x() > newPoint.x() || oldPoint.y() > newPoint.y()) {
				sign = -1.f;
			}
			else {
				sign = 1.f;
			}
		}
		//     |
		//  -------
		//   x |
		else if (newPoint.x() <= tanCenter.x() && newPoint.y() <= tanCenter.y()) {
			if(oldPoint.x() > newPoint.x() || oldPoint.y() < newPoint.y()) {
				sign = -1.f;
			}
			else {
				sign = 1.f;
			}
		}
		//   x |
		//  -------
		//     |
		else {
			if(oldPoint.x() <= newPoint.x() || oldPoint.y() <= newPoint.y()) {
				sign = -1.f;
			}
			else {
				sign = 1.f;
			}
		}

		//new -> old
		double nox = newPoint.x() - oldPoint.x();
		double noy = newPoint.y() - oldPoint.y();
		//new -> center
		double ncx = newPoint.x() - tanCenter.x();
		double ncy = newPoint.y() - tanCenter.y();
		//old -> center
		double ocx = oldPoint.x() - tanCenter.y();
		double ocy = oldPoint.y() - tanCenter.y();

		double no = Math.sqrt(Math.pow(nox, 2) + Math.pow(noy, 2));
		double nc = Math.sqrt(Math.pow(ncx, 2) + Math.pow(ncy, 2));
		double oc = Math.sqrt(Math.pow(ocx, 2) + Math.pow(ocy, 2));

		double angle = sign * Math.acos(Math.toRadians((Math.pow(oc, 2) + Math.pow(nc, 2) - Math.pow(no, 2)) / (2 * nc * oc)));

		System.out.println("rotating : " + angle + " degrees.");

		Selection.instance().getSelectedTan().rotate(angle);
	}
}
