package Tools;

import Geometry.Point;

public abstract class Transformer {

	public abstract void controlMoved(Point oldPoint, Point newPoint);
}
