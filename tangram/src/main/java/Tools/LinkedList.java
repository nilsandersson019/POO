package Tools;

import java.util.*;

public class LinkedList<G> extends AbstractSequentialList<G> {

	private Node<G> head, tail;
	private int size;
	public LinkedList() {
		size = 0;
		head = new Node<>(null);
		tail = new Node<>(null);

		head.setNext(tail);
		head.setPrevious(head);
		tail.setNext(tail);
		tail.setPrevious(head);
	}

	@Override
	public int size() {
		return size;
	}

	public boolean add(G t) {
		Node<G> n = new Node<>(t);
		n.next = tail;
		n.previous = tail.previous;
		n.previous.next = n;
		tail.previous = n;
		size++;
		return true;
	}

	public boolean remove(Object o) {
		Node<G> n = head;
		while (n.next!=tail) {
			n = n.next;
			if (n.elem.equals(o)) {
				n.previous.next = n.next;
				n.next.previous = n.previous;
				size--;
				return true;
			}
		}
		return false;
	}

	@Override
	public G get(int index) {
		Node<G> n = head;
		while (index-->=0) {
			n = n.next;
		}
		return n.elem;
	}

	@Override
	public ListIterator<G> listIterator(int i) {
		Node<G> tmp = head;
		for (int a = i; a > 0; a--) {
			tmp = tmp.next;
		}
		return new Iterator<>(tmp);
	}

	public class Iterator<G> implements ListIterator<G> {
		Node<G> current;

		public Iterator(Node<G> current) {
			this.current = current;
		}

		@Override
		public boolean hasNext() {
			return current.next != current.next.next;
		}

		@Override
		public G next() {
			if (!hasNext()) {
				throw new Error("Out of the list !");
			}
			current = current.next;
			return current.elem;
		}

		@Override
		public boolean hasPrevious() {
			return current.previous != current.previous.previous;
		}

		@Override
		public G previous() {
			current = current.previous;
			return current.elem;
		}

		@Override
		public int nextIndex() {
			int idx = 1;
			Node<G> n = current;
			while (n.previous != n.previous.previous) {
				n = n.previous;
				idx++;
			}
			return idx;
		}

		@Override
		public int previousIndex() {
			return nextIndex()-2;
		}

		@Override
		public void remove() {
			current.previous.next = current.next;
			current.next.previous = current.previous;
			current = current.previous;
			size--;
		}

		@Override
		public void set(G e) {
			current.elem = e;
		}

		@Override
		public void add(G e) {
			Node<G> n = new Node<>(e);
			n.previous = current.previous;
			n.next = current;
			current.previous = n;
			n.previous.next = n;
			size++;
		}
	}

	private class Node<G> {
		private G elem;
		private Node<G> next, previous;

		private Node(G content) {
			elem = content;
		}

		private void setNext(Node<G> next) {
			this.next = next;
		}

		private void setPrevious(Node<G> previous) {
			this.previous = previous;
		}
	}


}
