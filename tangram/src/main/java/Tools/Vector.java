package Tools;

import oop.lib.Degrees;

/**
 * @author ingold, Nils Andersson
 */
public class Vector {

    private double dx;
    private double dy;

    public Vector(double dx, double dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public double dx() {
        return dx;
    }

    public double dy() {
        return dy;
    }

    public static Vector[] rectanglePath(double width, double height) {
        return getVectors(width, height);
    }

    public static Vector[] squarePath(double width) {
        Vector[] vects = new Vector[4];
        vects[0] = new Vector(width, 0);
        vects[1] = new Vector(0, width);
        vects[2] = new Vector(-width, 0);
        vects[3] = new Vector(0, -width);
        return vects;
    }

    public static Vector[] trianglePath(double width, double height, double topShift) {
        Vector[] vects = new Vector[2];
        vects[0] = new Vector(width, 0);
        vects[1] = new Vector(-width / 2 + topShift, height);
        return vects;
    }

    public static Vector[] rectTrianglePath(double width, double height) {
        Vector[] vects = new Vector[2];
        vects[0] = new Vector(width, 0);
        vects[1] = new Vector(0, height);
        return vects;
    }
    public static Vector[] tanParalellogramPath(double shortSide) {
        Vector[] vects = new Vector[4];
        vects[0] = new Vector(shortSide, 0);
        vects[1] = new Vector(shortSide, shortSide);
        vects[2] = new Vector(-shortSide, 0);
        vects[3] = new Vector(-shortSide, -shortSide);

        return vects;
    }

    public static Vector[] pentagonPath(double radius) {
        Vector[] vects = new Vector[4];
        for (int i = 0; i < 4; i++) {
            double dx = radius * Degrees.cos(i * 72);
            double dy = radius * Degrees.sin(i * 72);
            vects[i] = new Vector(dx, dy);
        }
        return vects;
    }

    public static Vector[] getVectors(double width, double height) {
        Vector[] vects = new Vector[3];
        vects[0] = new Vector(width, 0);
        vects[1] = new Vector(0, height);
        vects[2] = new Vector(-width, 0);
        return vects;
    }
}
