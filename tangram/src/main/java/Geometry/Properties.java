package Geometry;

/**
 * @author ingold, Nils Andersson
 */
public interface Properties {

	public abstract double area();

	public abstract Point centroid();

	public boolean contains(Point p);

}

