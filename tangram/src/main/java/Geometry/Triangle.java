package Geometry;

import Tools.Vector;

import java.awt.*;

/**
 * @author ingold
 */
public class Triangle extends Polygon {

	private Triangle(Vector[] path, Color color) {
		super(path, color);
	}

	private Triangle(Vector[] path, Color color, Point center) {
		super(path, color, center);
	}

	private Triangle(double bWidth, double height, double tShift, Color color) {
		this(toVectors(bWidth, height, tShift), color);
	}

	private Triangle(double bWidth, double height, double tShift, Color color, Point center) {
		this(toVectors(bWidth, height, tShift), color, center);
	}

	private static Vector[] toVectors(double baseWidth, double height, double topShift) {
		Vector[] vects = new Vector[2];
		vects[0] = new Vector(baseWidth, 0);
		vects[1] = new Vector(-baseWidth / 2 + topShift, height);
		return vects;
	}

//    private static Geometry.Point[] path(double baseWidth, double height, double topShift) {
//        Geometry.Point[] pts = new Geometry.Point[3];
//        pts[0] = new Geometry.Point(0, 0);
//        pts[1] = new Geometry.Point(baseWidth, 0);
//        pts[2] = new Geometry.Point(baseWidth / 2 + topShift, height);
//        return pts;
//    }

	public static class Ordinary extends Triangle {

		public Ordinary(double a, double b, double c, Color color) {
			super(a, height(a, b, c), shift(a, b, c), color);
		}

		public Ordinary(double a, double b, double c, Color color, Point center) {
			super(a, height(a, b, c), shift(a, b, c), color, center);
		}

		private static double height(double a, double b, double c) {
			return 2 * area(a, b, c) / a;
		}

		private static double shift(double a, double b, double c) {
			double h = height(a, b, c);
			return Math.sqrt(c * c - h * h) - a / 2;
		}

		private static double area(double a, double b, double c) {
			double p = semiPerimeter(a, b, c);
			return Math.sqrt(p * (p - a) * (p - b) * (p - c));
		}

		private static double semiPerimeter(double a, double b, double c) {
			return (a + b + c) / 2;
		}
	}

	public static class Equilateral extends Triangle {

		public Equilateral(double width, Color color) {
			super(width, width * Math.sqrt(3) / 2, 0, color);
		}

		public Equilateral(double width, Color color, Point center) {
			super(width, width * Math.sqrt(3) / 2, 0, color, center);
		}
	}

	public static class Isosceles extends Triangle {

		public Isosceles(double width, double height, Color Color, Point center) {
			super(width, height, 0, Color, center);
		}
	}

	public static class LeftRectangle extends Triangle {

		public LeftRectangle(double width, double height, Color color) {
			super(width, height, -width / 2, color);
		}

		public LeftRectangle(double width, double height, Color color, Point center) {
			super(width, height, -width / 2, color, center);
		}
	}

	public static class RightRectangle extends Triangle {

		public RightRectangle(double width, double height, Color Color) {
			super(width, height, width / 2, Color);
		}

		public RightRectangle(double width, double height, Color Color, Point center) {
			super(width, height, width / 2, Color, center);
		}
	}

	public static class IsoscelesRectangle extends Triangle {

		public IsoscelesRectangle(double width, Color color) {
			super(width, width / 2, 0, color);
		}

		public IsoscelesRectangle(double width, Color color, Point center) {
			super(width, width / 2, 0, color, center);
		}
	}

}
