package Geometry;

import java.awt.*;

/**
 * @author ingold, Nils Andersson
 */
public class Square extends Rectangle {

	public Square(double size, Color color) {
		super(size, size, color);
	}

	public Square(double size, Color color, Point point) {
		super(size, size, color, point);
	}

}
