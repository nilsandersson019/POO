package Geometry;

import Jama.Matrix;

public class Segment {

	private Point a;
	private Point b;

	public Segment(Point a, Point b) {
		this.a = a;
		this.b = b;
	}

	public double length(Segment s1) {
		return Math.sqrt((Math.pow(s1.a.x() - s1.b.x(), 2)) + Math.pow(s1.a.y() - s1.b.y(), 2));
	}

	public Matrix slope() {
		double[][] array = {{b.y() - a.y()}, {b.x() - a.x()}};
		return new Matrix(array);
	}

	static private double ccw(Point p1, Point p2, Point p3) {
		return (p2.x() - p1.x()) * (p3.y() - p1.y()) - (p2.x() - p1.y()) * (p3.x() - p1.x());

	}

	public boolean intersects(Segment s) {
		return ccw(a, b, s.a) * ccw(a, b, s.b) < 0 && ccw(s.a, s.b, a) * ccw(s.a, s.b, b) < 0;
	}

	/**
	 * Very black magic that will take you some time to understand. I know it's way overkill but it wouldn't be cool otherwise.
	 * Also this was done for series 06 and took several hours to half work, and then in series 7 there you go and show the simple method...
	 * @param s segment to check the intersection with
	 * @return true if an intersection is found
	 */
//	public boolean intersects(Geometry.Segment s){
//		double[][] Px00_array = {{  a.x()   ,   a.y()   },{     b.x()   ,   b.y()   }};
//		double[][] Px01_array = {{  a.x()   ,   1       },{     b.x()   ,   1       }};
//		double[][] Px10_array = {{  s.a.x() ,   s.a.y() },{     s.b.x() ,   s.b.y() }};
//		double[][] Px11_array = {{  s.a.x() ,   1       },{     s.b.x() ,   1       }};
//
//		double[][] px00_array = {{  a.x()   ,   1       },{     b.x()   ,   1       }};
//		double[][] px01_array = {{  a.y()   ,   1       },{     b.y()   ,   1       }};
//		double[][] px10_array = {{  s.a.x() ,   1       },{     s.b.x() ,   1       }};
//		double[][] px11_array = {{  s.a.y() ,   1       },{     s.b.y() ,   1       }};
//
//		Matrix Px00 = new Matrix(Px00_array);
//		Matrix Px01 = new Matrix(Px01_array);
//		Matrix Px10 = new Matrix(Px10_array);
//		Matrix Px11 = new Matrix(Px11_array);
//
//		Matrix px00 = new Matrix(px00_array);
//		Matrix px01 = new Matrix(px01_array);
//		Matrix px10 = new Matrix(px10_array);
//		Matrix px11 = new Matrix(px11_array);
//
//		double[][] detp_array = {{px00.det(),px01.det()},{px10.det(),px11.det()}};
//		Matrix detp = new Matrix(detp_array);
//		double det2 = detp.det();
//
//		if(det2 == 0) {
//
//			//GO FIGURE!
//			if((     Math.min(a.x(),b.x()) <= Math.max(s.a.x(), s.b.x()) || Math.min(s.a.x(), s.b.x()) <= Math.max(a.x(), b.x())) &&
//					(Math.min(a.y(),b.y()) <= Math.max(s.a.y(), s.b.y()) || Math.min(s.a.y(), s.b.y()) <= Math.max(a.y(), b.y()))) {
//				Tools.Vector[] asdlfkj = {new Tools.Vector(a.x(), a.y()),new Tools.Vector(b.x(), b.y()),new Tools.Vector(s.a.x(), s.a.y()),new Tools.Vector(s.b.x(), s.b.y())};
//				Double area = new Geometry.Polygon(asdlfkj, Color.BLACK).area();
//				if(area == 0.f || area.isNaN()) {
//					return true;
//				}
//			}
//
//
//			return false;
//		}
//
//		double[][] detP_array = {{Px00.det(), Px01.det()},{Px10.det(),Px11.det()}};
//		Matrix detP = new Matrix(detP_array);
//		double det1 = detP.det();
//
//		double intersectX = det1 / det2;
//			if(intersectX >= Math.max(Math.min(a.x(), b.x()), Math.min(s.a.x(), s.b.x())) && intersectX <= Math.min(Math.max(a.x(), b.x()), Math.max(s.a.x(), s.b.x()))) {
//				return true;
//			}
//		return false;
}

