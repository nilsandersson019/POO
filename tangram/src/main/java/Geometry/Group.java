package Geometry;

import Tools.LinkedList;
import oop.lib.Painting;

import java.util.Arrays;

public class Group extends Shape {

	LinkedList<Shape> groupList = new LinkedList<>();

	public Group(Shape... shapes) {
		super(null);
		groupList.addAll(Arrays.asList(shapes));

	}

	public void add(Shape s) {
		groupList.add(s);
	}

	@Override
	public Point centroid() {
		double x = 0, y = 0;
		for (Shape s : groupList) {
			x += s.centroid().x();
			y += s.centroid().y();
		}
		x /= groupList.size();
		y /= groupList.size();
		return new Point(x, y);
	}

	@Override
	public boolean contains(Point p) {
		for (Shape s : groupList) {
			if (s.contains(p)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void translate(double deltaX, double deltaY) {
		for (Shape s : groupList) {
			s.translate(deltaX, deltaY);
		}
	}

	@Override
	public void rotate(double angle, Point anchor) {
		for (Shape s : groupList) {
			s.rotate(angle, anchor);
		}
	}

	@Override
	public void scale(double factor, Point anchor) {
		for (Shape s : groupList) {
			s.scale(factor, anchor);
		}
	}

	@Override
	public void paint(Painting painting) {
		for (Shape s : groupList) {
			s.paint(painting);
		}
	}

	public LinkedList<Shape> getGroupList() {
		return groupList;
	}

	@Override
	public boolean isAloneInPuzzleField(Polygon puzzleField) {
		return groupList.get(0).isAloneInPuzzleField(puzzleField);
	}
}

