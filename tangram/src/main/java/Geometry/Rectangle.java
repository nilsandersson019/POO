package Geometry;

import Tools.Vector;

import java.awt.*;

/**
 * @author ingold
 */
public class Rectangle extends Polygon {

	public Rectangle(double width, double height, Color color) {
		super(toVectors(width, height), color);
	}

	public Rectangle(double width, double height, Color color, Point point) {
		super(toVectors(width, height), color, point);
	}

	private static Vector[] toVectors(double width, double height) {
		return Vector.getVectors(width, height);
	}

}
