package Geometry;

import Tools.LinkedList;
import oop.lib.Paintable;
import oop.lib.Painting;

import java.awt.*;

/**
 * @author ingold, Nils Andersson
 */
public abstract class Shape implements Paintable, Properties {

	//protected Point centroid;

	public Boolean intersectsWithShape(){
		return false;
	};

	public LinkedList toSegments() {
		return null;
	}

	@Override
	public double area() {
		return 0;
	}

	public abstract boolean contains(Point p);

	private Color color;

	public Shape(Color color) {
		if(color == null) {
			color = new Color(255,0,255);
		}
		this.color = color;
	}


	public Color getColor() {
		return color;
	}

	public abstract boolean isAloneInPuzzleField(Polygon puzzleField);

	public abstract void translate(double deltaX, double deltaY);

	public abstract void rotate(double angle, Point anchor);

	public final void rotate(double angle) {
		rotate(angle, centroid());
	}

	public abstract void scale(double factor, Point anchor);

	public final void scale(double factor) {
		scale(factor, centroid());
	}

}
