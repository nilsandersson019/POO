package Geometry;

import Tools.LinkedList;

import java.awt.*;

public class Tan extends Group {

	private Circle handle;

	public Tan(Shape polygon) {
		//TODO to get a nice border, i want to getVectors off of polygon
		//Polygon borderPol = new Polygon(polygon.getVertices() , Color.black, polygon.centroid())
		this.add(polygon);
		handle = new Circle(Color.BLACK, polygon.centroid(), 9);
		add(handle);
	}


	public boolean isOnSpot(Point pt) {
		return handle.contains(pt);
	}

	@Override
	public void translate(double deltaX, double deltaY) {
		System.out.println("handle pos before X:" + handle.centroid().x() + " Y:" + handle.centroid().y());
		getGroupList().get(0).translate(deltaX, deltaY);
		handle.translateTo(getGroupList().get(0).centroid());
		System.out.println("handle pos after X:" + handle.centroid().x() + " Y:" + handle.centroid().y());

	}

	@Override
	public void rotate(double angle, Point anchor) {
		double deltaX = centroid().x();
		double deltaY = centroid().y();

		getGroupList().get(0).rotate(angle, anchor);
		deltaX = centroid().x() - deltaX;
		deltaY = centroid().y() - deltaY;
		handle.translateTo(getGroupList().get(0).centroid());
	}

	@Override
	public void scale(double factor, Point anchor) {

	}

	public Point getHandleCenter() {
		return handle.centroid();
	}

	@Override
	public Point centroid() {
		return groupList.get(0).centroid();
	}

	public LinkedList<Shape> getTanGroupList() {
		return groupList;
	}

	public boolean isAloneInPuzzleField(Polygon puzzleField) {
		return groupList.get(0).isAloneInPuzzleField(puzzleField);
	}
}
