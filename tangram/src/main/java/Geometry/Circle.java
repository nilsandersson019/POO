package Geometry;

import oop.lib.Painting;

import java.awt.*;

public class Circle extends Shape {

	private Point centroid;
	private double radius;

	public Circle(Color color, Point centroid, double radius) {
		super(color);
		this.centroid = centroid;
		this.radius = radius;
	}

	public Circle(Color color) {
		super(color);
	}

	@Override
	public boolean isAloneInPuzzleField(Polygon puzzleField) {
		return true;
	}

	@Override
	public void translate(double deltaX, double deltaY) {
		centroid.translate(deltaX, deltaY);
	}


	public void translateTo(Point p) {
		centroid = p;
	}

	@Override
	public void rotate(double angle, Point anchor) {
		System.out.println("Circle successfully \"rotated\"");

	}

	@Override
	public void scale(double factor, Point anchor) {

	}

	public double radius(){
		return 0.f;
	}

	@Override
	public double area() {
		return Math.PI * Math.pow(radius,2);
	}

	@Override
	public Point centroid() {
		return centroid;
	}

	@Override
	public boolean contains(Point p) {
		return Math.abs(Math.sqrt(Math.pow(centroid().x() - p.x(), 2) + Math.pow(centroid().y() - p.y(),2))) <= radius();
	}

	@Override
	public void paint(Painting painting) {
		painting.setColor(getColor());
		painting.fillCircle(centroid.asArray(), radius);
	}
}
