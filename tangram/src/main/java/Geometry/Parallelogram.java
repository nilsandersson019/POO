package Geometry;

import Tools.Vector;

import java.awt.*;

/**
 * @author ingold
 */
public class Parallelogram extends Polygon {

	public Parallelogram(double bWidth, double height, double tShift, Color color) {
		super(toVectors(bWidth, height, tShift), color);
	}

	public Parallelogram(double bWidth, double height, double tShift, Color color, Point center) {
		super(toVectors(bWidth, height, tShift), color, center);
	}

	private static Vector[] toVectors(double width, double height, double shift) {
		Vector[] vects = new Vector[3];
		vects[0] = new Vector(width, 0);
		vects[1] = new Vector(shift, height);
		vects[2] = new Vector(-width, 0);
		return vects;
	}

}
