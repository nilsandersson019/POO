package Runnable;

import Geometry.Point;
import Tools.LinkedList;
import Tools.Vector;
import oop.lib.Paintable;
import oop.project.Bench;

import java.awt.*;

public class TestOfContains extends Bench {

	Geometry.Polygon rectangle1, rectangle2, rectangle3, triangle1, pentagon1;
	int count = 0;

	private LinkedList<Paintable> paintableList;
	private LinkedList<Geometry.Shape> shapeList;
//	private ArrayList<Geometry.Shape> shapeList;
//	private ArrayList<Paintable> paintableList;

	public static void main(String[] args) {
		new TestOfContains();
	}

	@Override
	public void init() {

		Vector[] rectPath1 = Vector.rectanglePath(2, 1);
		Vector[] rectPath2 = Vector.rectanglePath(1, 0.6);
		Vector[] trigPath1 = Vector.trianglePath(2, 0.757, 0);
		Vector[] pentPath1 = Vector.pentagonPath(0.5);
		rectangle1 = new Geometry.Polygon(rectPath1, Color.YELLOW, new Geometry.Point(0, 0));
		rectangle2 = new Geometry.Polygon(rectPath2, Color.CYAN, new Geometry.Point(1.2, 1.2));
		rectangle3 = new Geometry.Polygon(rectPath2, Color.GREEN, new Geometry.Point(-1.2, -1.2));
		triangle1 = new Geometry.Polygon(trigPath1, Color.RED, new Geometry.Point(-0, 0));
		pentagon1 = new Geometry.Polygon(pentPath1, Color.BLUE, new Geometry.Point(0, 0));

		shapeList = new LinkedList<Geometry.Shape>();
		//shapeList = new ArrayList<Geometry.Shape>();
		shapeList.add(rectangle1);
		shapeList.add(rectangle2);
		shapeList.add(rectangle3);
		shapeList.add(triangle1);
		shapeList.add(pentagon1);
		paintableList = new LinkedList<Paintable>();
		//paintableList = new ArrayList<Paintable>();
		paintableList.add(rectangle1);
		paintableList.add(rectangle2);
		paintableList.add(rectangle3);
		paintableList.add(triangle1);
		paintableList.add(pentagon1);

		defineCoordinates(getPanel().getWidth() / 2, getPanel().getHeight() / 2, 100, true);

		getPanel().setContent(paintableList);
	}

	@Override
	public void step() {
		count++;
		rectangle1.rotate(15);
		triangle1.rotate(15, triangle1.getVertex(0));
		pentagon1.rotate(15);
		rectangle2.scale(count % 2 == 1 ? 0.8 : 1.265);
		rectangle3.translate(0.1, 0);
		if (count % 24 == 0) {
			rectangle3.translate(-2.4, 0);
		}

	}

	@Override
	public void click(double x, double y) {
		System.out.println("X:" + x + " Y:" + y);

		for (int i = paintableList.size() - 1; i >= 0; i--) {
			Geometry.Point p = new Point(x, y);


			Geometry.Shape s = shapeList.get(i);
			if (s.contains(p)) {
				Geometry.Shape tmp = (Geometry.Shape) paintableList.get(i);

				paintableList.remove(i);
				shapeList.remove(i);

				paintableList.add(tmp);
				shapeList.add(tmp);

				getPanel().setContent(paintableList);

				step();
				return;
			}
		}
	}

}


