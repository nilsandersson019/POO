package Runnable;

import Geometry.Polygon;
import Geometry.Tan;
import Tools.LinkedList;
import Tools.TanList;
import Tools.Vector;
import UI.Selection;
import UI.View;
import UI.Window;

import javax.swing.*;
import java.awt.*;

public class Main {

	public static final int FIELD_WIDTH = 600;
	public static final int MARGIN = 10*View.STEP_SIZE;


	private static Window w;
	private static Selection s;

	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | UnsupportedLookAndFeelException | IllegalAccessException e) {
			e.printStackTrace();
		}

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				w = new UI.Window(FIELD_WIDTH + 2 * MARGIN, 2 * (FIELD_WIDTH + MARGIN));
			}
		});
	}

	public static UI.Window getWindow() {
		return w;
	}

	public static void setWindow(Window window) {
		w = window;
	}

}