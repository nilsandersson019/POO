package Runnable;

import Geometry.Point;
import Tools.LinkedList;
import Tools.Vector;
import oop.lib.Degrees;
import oop.project.Bench;

import java.awt.*;


/**
 * @author ingold
 */
public class Test4 extends Bench {

	Geometry.Polygon rectangle1, rectangle2, rectangle3, triangle1, pentagon1;
	int count = 0;

	public static void main(String[] args) {
		new Test4();
		System.out.println("##################################################");
        LinkedList<String> bob = new LinkedList<>();
        bob.add("asdfkjh");
        bob.add("qwer");
        bob.add("uio");
        bob.add("zukhjggj");
        bob.add("yxcvcxvbyxcv");
        bob.add("mmmmm");
        bob.add("Schmetterling");

        for(String s : bob) {
	        System.out.println(s);
        }
	}

	private static Vector[] trianglePath(double width, double height, double topShift) {
		Vector[] vects = new Vector[2];
		vects[0] = new Vector(width, 0);
		vects[1] = new Vector(-width / 2 + topShift, height);
		return vects;
	}

	private static Vector[] rectanglePath(double width, double height) {
		return Vector.getVectors(width, height);
	}

	private static Vector[] pentagonPath(double radius) {
		Vector[] vects = new Vector[4];
		for (int i = 0; i < 4; i++) {
			double dx = radius * Degrees.cos(i * 72);
			double dy = radius * Degrees.sin(i * 72);
			vects[i] = new Vector(dx, dy);
		}
		return vects;
	}

	@Override
	public void init() {
		defineCoordinates(getPanel().getWidth() / 2, getPanel().getHeight() / 2, 100, true);
		Vector[] rectPath1 = rectanglePath(2, 1);
		Vector[] rectPath2 = rectanglePath(1, 0.6);
		Vector[] trigPath1 = trianglePath(2, 0.75, 0);
		Vector[] pentPath1 = pentagonPath(0.5);
		rectangle1 = new Geometry.Polygon(rectPath1, Color.YELLOW, new Geometry.Point(0, 0));
		rectangle2 = new Geometry.Polygon(rectPath2, Color.CYAN, new Geometry.Point(1.2, 1.2));
		rectangle3 = new Geometry.Polygon(rectPath2, Color.GREEN, new Geometry.Point(-1.2, -1.2));
		triangle1 = new Geometry.Polygon(trigPath1, Color.RED, new Geometry.Point(-0, 0));
		pentagon1 = new Geometry.Polygon(pentPath1, Color.BLUE, new Point(0, 0));
		add(rectangle1);
		add(rectangle2);
		add(rectangle3);
		add(triangle1);
		add(pentagon1);
	}

	@Override
	public void step() {
		count++;
		rectangle1.rotate(15);
		triangle1.rotate(15, triangle1.getVertex(0));
		pentagon1.rotate(15);
		rectangle2.scale(count % 2 == 1 ? 0.8 : 1.25);
		rectangle3.translate(0.1, 0);
		if (count % 24 == 0) {
			rectangle3.translate(-2.4, 0);
		}
	}

}
