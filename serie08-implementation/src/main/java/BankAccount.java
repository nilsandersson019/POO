public class BankAccount {
	double balance;

	public BankAccount(double balance) {
		this.balance = balance;
	}

	public double withdraw(double amount) throws insufficientFundsException {
		{

			if (balance >= amount) {
				double newBalance = this.balance - amount;
				this.balance -= amount;

				return newBalance;
			}

			throw new insufficientFundsException();

		}
	}
}
